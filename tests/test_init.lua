-- smoke test to ensure that requiring the main
-- init.lua file won't lead to syntax errors.

Tracker = {
    ActiveVariantUID = 'standard_with_map',
}

ScriptHost = {
}

Item = {
    Active = false,
    Name = '',
    CurrentStage = '',
}

function Item:create(o)
    return o
end

function ScriptHost:LoadScript(filename)
    local imported = require(filename:gsub('/', '.'):sub(1, -5))
    assert(imported)
end

function ScriptHost:AddMemoryWatch(name, addr, length, callback, rate)
end

function Tracker:AddItems(filename)
end

function Tracker:AddMaps(filename)
end

function Tracker:AddLocations(filename)
end

function Tracker:AddLayouts(filename)
end

function Tracker:FindObjectForCode(codename)
    return Item:create{}
end

local function test_init_works()
    local imported = require 'scripts/init'
    assert(imported)
end

test_init_works()
