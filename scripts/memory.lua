debug_log("emotracker", "memory load")

mem_addr                = {
    ["gameversion"]     = { ["name"]="Game Version",             ["offset"]=0x80000AC,  ["len"]=2,   ["callback"]=_check_game_version,  ["rate"]=5000, },
    ["gamemode"]        = { ["name"]="Game Mode",                ["offset"]=0x3000BDE,  ["len"]=2,   ["callback"]=_update_game_mode,    ["rate"]=50, },
    ["area"]            = { ["name"]="Current Area",             ["offset"]=0x300002C,  ["len"]=1,   ["callback"]=_update_game_area,    ["rate"]=50, },
    ["boss"]            = { ["name"]="Boss Defeated",            ["offset"]=0x30006BA,  ["len"]=2,   ["callback"]=_update_boss,         ["rate"]=250, },
    ["beam"]            = { ["name"]="Beam Status",              ["offset"]=0x300131A,  ["len"]=1,   ["callback"]=_update_beam,         ["rate"]=250, },
    ["weapons"]         = { ["name"]="Bombs and Missile Status", ["offset"]=0x300131B,  ["len"]=1,   ["callback"]=_update_weapons,      ["rate"]=250, },
    ["suit"]            = { ["name"]="Suit and Misc Status",     ["offset"]=0x300131C,  ["len"]=1,   ["callback"]=_update_suit,         ["rate"]=250, },
    ["security"]        = { ["name"]="Security Status",          ["offset"]=0x300131D,  ["len"]=1,   ["callback"]=_update_security,     ["rate"]=250, },
    ["health"]          = { ["name"]="E-Tank",                   ["offset"]=0x3001312,  ["len"]=2,   ["callback"]=_update_etank,        ["rate"]=250, },
    ["missile_count"]   = { ["name"]="Missile Tank",             ["offset"]=0x3001316,  ["len"]=2,   ["callback"]=_update_mtank,        ["rate"]=250, },
    ["pb_count"]        = { ["name"]="Power Bomb Tank",          ["offset"]=0x3001319,  ["len"]=1,   ["callback"]=_update_ptank,        ["rate"]=250, },
    ["s0_tanks"]        = { ["name"]="Main Deck Items",          ["offset"]=0x2037200,  ["len"]=52,  ["callback"]=_update_s0_tanks,     ["rate"]=250, },
    ["s1_tanks"]        = { ["name"]="Sector 1 Items",           ["offset"]=0x2037400,  ["len"]=48,  ["callback"]=_update_s1_tanks,     ["rate"]=250, },
    ["s2_tanks"]        = { ["name"]="Sector 2 Items",           ["offset"]=0x2037500,  ["len"]=68,  ["callback"]=_update_s2_tanks,     ["rate"]=250, },
    ["s3_tanks"]        = { ["name"]="Sector 3 Items",           ["offset"]=0x2037600,  ["len"]=64,  ["callback"]=_update_s3_tanks,     ["rate"]=250, },
    ["s4_tanks"]        = { ["name"]="Sector 4 Items",           ["offset"]=0x2037700,  ["len"]=60,  ["callback"]=_update_s4_tanks,     ["rate"]=250, },
    ["s5_tanks"]        = { ["name"]="Sector 5 Items",           ["offset"]=0x2037800,  ["len"]=60,  ["callback"]=_update_s5_tanks,     ["rate"]=250, },
    ["s6_tanks"]        = { ["name"]="Sector 6 Items",           ["offset"]=0x2037900,  ["len"]=48,  ["callback"]=_update_s6_tanks,     ["rate"]=250, },
}

boss                    = {
    ["arachnus"]        = { ["value"]=0x0001,   ["location"]="@S0-18-08",   ["status"]=false, },
    ["charge_core_x"]   = { ["value"]=0x0002,   ["location"]="@S1-09-06",   ["status"]=false, },
    ["wide_core_x"]     = { ["value"]=0x0004,   ["location"]="@S3-03-08",   ["status"]=false, },
    ["nettori"]         = { ["value"]=0x0008,   ["location"]="@S2-0D-06",   ["status"]=false, },
    ["box2"]            = { ["value"]=0x0010,   ["location"]="@S6-07-06",   ["status"]=false, },
    ["zazabi"]          = { ["value"]=0x0020,   ["location"]="@S2-0E-0D",   ["status"]=false, },
    ["serris"]          = { ["value"]=0x0040,   ["location"]="@S4-0A-00",   ["status"]=false, },
    ["varia_core_x"]    = { ["value"]=0x0080,   ["location"]="@S6-0B-0B",   ["status"]=false, },
    ["yakuza"]          = { ["value"]=0x0100,   ["location"]="@S0-15-15",   ["status"]=false, },
    ["nightmare"]       = { ["value"]=0x0200,   ["location"]="@S5-16-06",   ["status"]=false, },
    ["ridley"]          = { ["value"]=0x0400,   ["location"]="@S1-01-09",   ["status"]=false, },
}

beam                            = {
    ["charge_beam"]             = { ["value"]=0x01,  ["status"]=false, },
    ["wide_beam"]               = { ["value"]=0x02,  ["status"]=false, },
    ["plasma_beam"]             = { ["value"]=0x04,  ["status"]=false, },
    ["wave_beam"]               = { ["value"]=0x08,  ["status"]=false, },
    ["ice_beam"]                = { ["value"]=0x10,  ["status"]=false, },
}

weapons                         = {
    ["missile_data"]            = { ["value"]=0x01, ["status"]=false, },
    ["super_missile_data"]      = { ["value"]=0x02, ["status"]=false, },
    ["ice_missile_data"]        = { ["value"]=0x04, ["status"]=false, },
    ["diffusion_missile_data"]  = { ["value"]=0x08, ["status"]=false, },
    ["bomb_data"]               = { ["value"]=0x10, ["status"]=false, },
    ["power_bomb_data"]         = { ["value"]=0x20, ["status"]=false, },
}

suit                            = {
    ["high_jump"]               = { ["value"]=0x0001,   ["status"]=false, },
    ["speed_booster"]           = { ["value"]=0x0002,   ["status"]=false, },
    ["space_jump"]              = { ["value"]=0x0004,   ["status"]=false, },
    ["screw_attack"]            = { ["value"]=0x0008,   ["status"]=false, },
    ["varia_suit"]              = { ["value"]=0x0010,   ["status"]=false, },
    ["gravity_suit"]            = { ["value"]=0x0020,   ["status"]=false, },
    ["morph_ball"]              = { ["value"]=0x0040,   ["status"]=false, },
}

security                        = {
    ["door_lock_1"]             = { ["value"]=0x02,  ["status"]=false, },
    ["door_lock_2"]             = { ["value"]=0x04,  ["status"]=false, },
    ["door_lock_3"]             = { ["value"]=0x08,  ["status"]=false, },
    ["door_lock_4"]             = { ["value"]=0x10,  ["status"]=false, },
}

broadcast_security              = {
    ["broadcast_lock_1"]        = { ["value"]=0x02,  ["status"]=false, },
    ["broadcast_lock_2"]        = { ["value"]=0x04,  ["status"]=false, },
    ["broadcast_lock_3"]        = { ["value"]=0x08,  ["status"]=false, },
    ["broadcast_lock_4"]        = { ["value"]=0x10,  ["status"]=false, },
}

minor_items                     = {
    ["energy_tank"]             = 0,
    ["missile_tank"]            = 0,
    ["pb_tank"]                 = 0,
}

item_locations      = {
    ["tank_id"]     = {
        [0x01] = "missile_tank",
        [0x02] = "energy_tank",
        [0x03] = "pb_tank",
        [0x04] = "missile_data",
        [0x05] = "morph_ball",
        [0x06] = "charge_beam",
        [0x07] = "bomb_data",
        [0x08] = "high_jump",
        [0x09] = "speed_booster",
        [0x0A] = "super_missile_data",
        [0x0B] = "varia_suit",
        [0x0C] = "ice_missile_data",
        [0x0D] = "wide_beam",
        [0x0E] = "power_bomb_data",
        [0x0F] = "space_jump",
        [0x10] = "plasma_beam",
        [0x11] = "gravity_suit",
        [0x12] = "diffusion_missile_data",
        [0x13] = "wave_beam",
        [0x14] = "screw_attack",
        [0x15] = "ice_beam",
    },
        -- RoomID = { [XXYY] = "Tracker Location Name" }, where list ofX/Y are coordinates
        -- Tracker noames are based on vanilla items
        ["sector_0"] = {
        [0x07] = { [0x0E0D] = "@S0-0E-0B" }, --Room "@Main Deck/S0-0A-13"
        [0x11] = { [0x1409] = "@S0-05-16" }, --Room "@Main Deck/S0-05-16"
        [0x23] = { [0x410E] = "@S0-14-07" }, --Room "@Main Deck/S0-14-07"
        [0x26] = { [0x0A35] = "@S0-18-06" }, --Room "@Main Deck/S0-18-08"
        [0x2D] = { [0x0604] = "@S0-13-07" }, --Room "@Main Deck/S0-13-07"
        [0x2F] = { [0x0304] = "@S0-08-0B" }, --Room "@Main Deck/S0-08-0B"
        [0x32] = { [0x0836] = "@S0-15-10" }, --Room "@Main Deck/S0-15-10"
        [0x33] = { [0x1D05] = "@S0-16-12" }, --Room "@Main Deck/S0-16-12"
        [0x39] = { [0x0A0C] = "@S0-0E-07" }, --Room "@Main Deck/S0-0E-07"
        [0x45] = { [0x1D1D] = "@S0-09-04" }, --Room "@Main Deck/S0-09-04"
        [0x48] = { [0x090D] = "@S0-0C-09" }, --Room "@Main Deck/S0-0C-09"
        [0x49] = { [0x0A06] = "@S0-05-08" }, --Room "@Main Deck/S0-04-08"
        [0x54] = { [0x0A0E] = "@S0-19-06" }, --Room "@Main Deck/S0-19-06"
    },
    ["sector_1"] = {
        [0x05] = { [0x0A1B] = "@S1-0D-02" }, --1B0A
        [0x11] = {
            [0x0608] = "@S1-05-03", --0806
            [0x0819] = "@S1-06-03", --1908
            [0x132C] = "@S1-07-04", --2C13
        },
        [0x1E] = { [0x080F] = "@S1-03-0A" }, --0F08
        [0x27] = { [0x0604] = "@S1-07-00" }, --0406
        [0x28] = { [0x080C] = "@S1-09-04" }, --0C08
        [0x2B] = { [0x0B0D] = "@S1-0A-04" }, --0D0B
        [0x2C] = { [0x0804] = "@S1-0D-08" }, --0408
        [0x2F] = { [0x020A] = "@S1-0C-07" }, --0A02
        [0x32] = { [0x0806] = "@S1-11-02" }, --0608
        [0x34] = { [0x070D] = "@S1-08-0B" }, --0D07
    },
    ["sector_2"] = {
        [0x06] = { [0x081D] = "@S2-09-03" }, --1D08
        [0x09] = { [0x040D] = "@S2-00-05" }, --0D04
        [0x0A] = { [0x2313] = "@S2-02-0E" }, --1323
        [0x11] = { [0x072C] = "@S2-0C-0B" }, --2C07
        [0x15] = { [0x041D] = "@S2-0C-04" }, --1D04
        [0x19] = { [0x0804] = "@S2-03-0C" }, --0408
        [0x1B] = { [0x071C] = "@S2-09-05" }, --1C07
        [0x1F] = { [0x0728] = "@S2-04-04" }, --2807
        [0x21] = { [0x0815] = "@S2-04-03" }, --1508
        [0x2A] = { [0x0805] = "@S2-04-0B" }, --0508
        [0x2F] = { [0x101D] = "@S2-08-08" }, --1D10
        [0x32] = {
            [0x0703] = "@S2-05-08",
            [0x1803] = "@S2-05-0A", -- 0318
        },
        [0x36] = {
            [0x0504] = "@S2-05-00", --0405
            [0x0E09] = "@S2-05-01", --090E
        },
        [0x37] = {
            [0x0407] = "@S2-10-0C", --0704
            [0x1A0A] = "@S2-10-0E", --0A1A
        },
    },
    ["sector_3"] = {
        [0x03] = { [0x0D2C] = "@S3-06-06" }, --2C0D
        [0x06] = { [0x1105] = "@S3-0B-04" }, --0511
        [0x08] = {
            [0x0909] = "@S3-0A-01", --0909
            [0x0D16] = "@S3-0B-02", --160D
        },
        [0x09] = { [0x082A] = "@S3-0F-00" }, --2A08
        [0x0C] = { [0x190C] = "@S3-07-0B" }, --0C19
        [0x13] = {
            [0x0D13] = "@S3-11-0A", --130D
            [0x0A2B] = "@S3-12-09", --2B0A
        },
        [0x1C] = {
            [0x070C] = "@S3-01-02", --0C07
            [0x1B24] = "@S3-03-04", --241B
        },
        [0x1E] = { [0x0D04] = "@S3-0B-06" }, --040D
        [0x21] = { [0x0A0F] = "@S3-11-04" }, --0F0A
        [0x22] = { [0x0F0A] = "@S3-0E-0A" }, --0A0F
        [0x23] = {
            [0x1B04] = "@S3-14-03", --041B
            [0x560F] = "@S3-14-09", --0F56
        },
        [0x25] = { [0x030F] = "@S3-00-05" }, --0F03
    },
    ["sector_4"] = {
        [0x06] = { [0x1D16] = "@S4-0C-06" }, --161D
        [0x0A] = { [0x1D0C] = "@S4-09-02" }, --0C1D
        [0x0D] = {
            [0x0918] = "@S4-0D-01", --1809
            [0x0F26] = "@S4-0E-02", --260F
        },
        [0x0F] = { [0x052C] = "@S4-0B-08" }, --2C05
        [0x11] = { [0x1417] = "@S4-0A-0C" }, --1714
        [0x17] = { [0x1339] = "@S4-07-08" }, --3913
        [0x18] = { [0x0728] = "@S4-07-0A" }, --2807
        [0x1C] = { [0x0609] = "@S4-09-06" }, --0906
        [0x21] = { [0x0D0A] = "@S4-00-06" }, --0A0D
        [0x24] = { [0x0703] = "@S4-0F-06" }, --0307
        [0x26] = {
            [0x0A16] = "@S4-12-07", --160A
            [0x052A] = "@S4-13-07", --2A05
        },
        [0x29] = { [0x040F] = "@S4-06-0E" }, --0F04
        [0x2E] = { [0x0A04] = "@S4-05-03" }, --040A
    },
    ["sector_5"] = {
        [0x04] = {
            [0x0505] = "@S5-04-01", --0505
            [0x0814] = "@S5-05-01", --1408
        },
        [0x0C] = { [0x0A03] = "@S5-0C-07" }, --030A
        [0x0E] = { [0x030D] = "@S5-0F-07" }, --0D03
        [0x12] = { [0x0303] = "@S5-12-04" }, --0303
        [0x16] = { [0x3003] = "@S5-07-0B" }, --0330
        [0x17] = { [0x060E] = "@S5-08-07" }, --0E06
        [0x1A] = { [0x0604] = "@S5-06-05" }, --0406
        [0x1E] = { [0x0717] = "@S5-03-04" }, --1707
        [0x21] = { [0x030E] = "@S5-05-04" }, --0E03
        [0x22] = { [0x080E] = "@S5-16-04" }, --0E08
        [0x24] = { [0x0808] = "@S5-0B-01" }, --0808
        [0x2F] = { [0x0A04] = "@S5-11-05" }, --040A
        [0x32] = { [0x080D] = "@S5-14-07" }, --0D08
        [0x33] = { [0x030B] = "@S5-0E-08" }, --0B03
    },
    ["sector_6"] = {
        [0x00] = { [0x1229] = "@S6-05-03" }, --2912
        [0x0F] = { [0x0303] = "@S6-05-0B" }, --0303
        [0x12] = {
            [0x030F] = "@S6-09-05", --0F03
            [0x141D] = "@S6-0A-06", --1D14
        },
        [0x18] = { [0x091D] = "@S6-0E-03" }, --1D09
        [0x1A] = { [0x0605] = "@S6-06-08" }, --0506
        [0x1E] = {
            [0x0D09] = "@S6-0B-09", --090D
            [0x0813] = "@S6-0C-08", --1308
        },
        [0x22] = { [0x080E] = "@S6-08-03" }, --0E08
        [0x26] = { [0x062D] = "@S6-0E-04" }, --2D06
        [0x27] = {
            [0x180A] = "@S6-01-06", --0A18
            [0x0A21] = "@S6-03-04", --210A
        },
    }
}
