function has_item(itemref)
    if Tracker:ProviderCountForCode(itemref) >= 1 then
        return true
    else
        return false
    end
end

function missing_item(itemref)
    return not has_item(itemref)
end

function Badge(locref, img)
    local location = Tracker:FindObjectForCode(locref)
    location:AddBadge(img)
end


function ripper_tower()
    local highjump  = Tracker:FindObjectForCode("high_jump")
    local spacejump = Tracker:FindObjectForCode("space_jump")
    local plasma    = Tracker:FindObjectForCode("plasma_beam")

    if not highjump.Active or (spacejump.Active and plasma.Active) then
        return true
    else
        return false
    end
end
