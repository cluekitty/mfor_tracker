debug_log("emotracker", "functions load")

function _cb(value)
    debug_log("callback",tostring(value))
    return true
end

function _check_game_version(segment)
    local gameversion = read_long(mem_addr["gameversion"]["offset"])
    local english_ver = 0x45544D41
     if gameversion == english_ver then
        debug_log("callback","Game Loaded Successfully")
        return true
    else
        print("Error: Incorrect Game detected. Expected USA/AUS ROM (AMTE), got: ".. string.format("%X",gameversion))
        AutoTracker:Stop()
        return false
    end
end

function _update_game_mode(segment)
    gamemode = read_short(mem_addr["gamemode"]["offset"])
    debug_log("updategamemode: gamemode updated to", gamemode)
end

function _update_game_area(segment)
    area = read_byte(mem_addr["area"]["offset"])
    debug_log("updategamearea: area updated to", area)
end

function is_in_game()
    debug_log("gamemode", gamemode)
    local title,ingame,reset,map,cutscene,closeup,filedelete,fileselect,gameover,ending,planetcrash,credits,demo = 0x0, 0x1, 0x2, 0x3, 0x4, 0x5, 0x6, 0x7, 0x8, 0x9, 0xA, 0xB, 0xC
    if (gamemode == ingame or
        gamemode == map or
        gamemode == cuscene or
        gamemode == ending or
        gamemode == credits
    ) then
        resetflag = false
        return true
    elseif (gamemode == reset or
            gamemode == filedelete or
            gamemode == fileselect or
            gamemode == title
    ) then
        reset_all(resetflag)
        return false
    else
        return false
    end
end

function update_samus(segment,status,samus,origin)
    debug_log("updatesamus", "status="..status)
    for k,v in pairs(samus) do
        local item = Tracker:FindObjectForCode(k)
        samus[k]["status"] = (status & samus[k]["value"]) == samus[k]["value"]
        item.Active = samus[k]["status"]
        if k == "missile_data" then
            _update_mtank()
        elseif k == "power_bomb_data" then
            _update_ptank()
        elseif origin == "boss" then
            update_boss_location(segment,k)
        end
        debug_log("updatesamus",k .. "=".. tostring(samus[k]["status"]))
    end
end

function update_tile(segment,sector,tile,roomid,tankid,coords)

    debug_log("update_tile", sector)
    debug_log("update_tile", tile)
    debug_log("update_tile", string.format("%x",roomid))
    debug_log("update_tile", string.format("%x",tankid))
    debug_log("update_tile", string.format("%x",coords))
    debug_log("update_tile", item_locations[sector][roomid][coords])

    if is_in_game() then
        local location = Tracker:FindObjectForCode(tostring(item_locations[sector][roomid][coords]))

        location.Sections.Name.AvailableChestCount = 0
        debug_log("update_tile: location:", location)
        debug_log("update_tile: location:", location.Sections.Name)
    end

end

function get_sector_tanks(addr,length)
    if is_in_game() then
        local result = {}
        -- we need to divide by 4 to only loop the correct number of times
        for i=0, (length / 4)-1 do
            local offset = i*4
            result[addr+offset]  = {
                --  Read through each byte in the address
                ["roomid"]  = read_byte( addr + offset    ),
                ["tankid"]  = read_byte( addr + offset + 1),
                ["coords"]  = read_short(addr + offset + 2),
            }
            debug_log("getsectortanks", addr+offset)
        end
        return result
    end
end

function update_boss_location(segment,name)
    debug_log("updatebosslocation", name)
    if is_in_game() then
        local location = Tracker:FindObjectForCode(boss[name]["location"])
        location.Sections.Name.AvailableChestCount = 0
    end
end

function _update_boss(segment)
    debug_log("callback","update boss")
    if is_in_game() then
        local status = read_short(mem_addr["boss"]["offset"])
        update_samus(segment,status,boss,"boss")
    end
end

function _update_beam(segment)
    debug_log("callback","update beam")
    if is_in_game() then
        local status = read_byte(mem_addr["beam"]["offset"])
        update_samus(segment,status,beam,"beam")
    end
end

function _update_weapons(segment)
    debug_log("callback","update weapons")
    if is_in_game() then
        local status = read_byte(mem_addr["weapons"]["offset"])
        update_samus(segment,status,weapons,"weapons")
    end
end

function _update_suit(segment)
    debug_log("callback","update suit")
    if is_in_game() then
        local status = read_byte(mem_addr["suit"]["offset"])
        update_samus(segment,status,suit,"suit")
    end
end

function _update_security(segment)
    debug_log("callback","update security")
    if is_in_game() then
        local status = read_byte(mem_addr["security"]["offset"])
        update_samus(segment,status,security,"security")
        update_samus(segment,status,broadcast_security,"security")
    end
end

function _update_etank(segment)
    debug_log("callback","update etank")
    if is_in_game() then
        -- Num of E-Tanks = floor(Max Health / 100)
        local etank = read_short(mem_addr["health"]["offset"]) // 100
        local etank_item = Tracker:FindObjectForCode("energy_tank")
        etank_item.AcquiredCount = etank
        debug_log("minor","etank="..tostring(etank))
    end
end

function _update_mtank(segment)
    debug_log("callback","update mtank")
    if is_in_game() then
        local mtank_item = Tracker:FindObjectForCode("missile_tank")
        if weapons["missile_data"]["status"] then
            local mtank = read_short(mem_addr["missile_count"]["offset"])
            mtank_item.AcquiredCount = mtank
            debug_log("minor","mstatus=" .. tostring(weapons["missile_data"]["status"]) .. "mtank="..tostring(mtank))
        else
            -- Don't display item count if you don't have main data
            mtank_item.AcquiredCount = 0
        end
    end
end

function _update_ptank(segment)
    debug_log("callback","update ptank")
    if is_in_game() then
        local ptank_item = Tracker:FindObjectForCode("pb_tank")
        if weapons["power_bomb_data"]["status"] then
            local ptank = read_byte (mem_addr["pb_count"]["offset"])
            ptank_item.AcquiredCount = ptank
            debug_log("minor","ptank="..tostring(ptank))
        else
            -- Don't display item count if you don't have main data
            ptank_item.AcquiredCount = 0
        end
    end
end

function _update_s0_tanks(segment)
    debug_log("callback","update s0 tank")
    debug_log("s0tanks: area",area)
    if maplayout then
        if is_in_game() and area == 0 then
            local tiles = get_sector_tanks(mem_addr["s0_tanks"]["offset"], mem_addr["s0_tanks"]["len"])

            --debug
            for offset,data in pairs(tiles) do
                debug_log("s0tank-loop",string.format("%x",offset).."=")
                debug_log("s0tank-loop",data["roomid"])
                debug_log("s0tank-loop",data["tankid"])
                debug_log("s0tank-loop",data["coords"])

                if data["roomid"] ~= 0xFF and data["tankid"] ~= 0xFF and data["coords"] ~= 0xFFFF then
                    update_tile(segment,"sector_0",tiles,data["roomid"],data["tankid"],data["coords"])
                end
            end
        end
    end
end

function _update_s1_tanks(segment)
    debug_log("callback","update s1 tank")
    debug_log("s0tanks: area",area)
    if maplayout then
        if is_in_game() and area == 1 then
            local tiles = get_sector_tanks(mem_addr["s1_tanks"]["offset"], mem_addr["s1_tanks"]["len"])

            --debug
            for offset,data in pairs(tiles) do
                debug_log("s1tank-loop",string.format("%x",offset).."=")
                debug_log("s1tank-loop",data["roomid"])
                debug_log("s1tank-loop",data["tankid"])
                debug_log("s1tank-loop",data["coords"])

                if data["roomid"] ~= 0xFF and data["tankid"] ~= 0xFF and data["coords"] ~= 0xFFFF then
                    update_tile(segment,"sector_1",tiles,data["roomid"],data["tankid"],data["coords"])
                end
            end
        end
    end
end

function _update_s2_tanks(segment)
    debug_log("callback","update s2 tank")
    debug_log("s0tanks: area",area)
    if maplayout then
        if is_in_game() and area == 2 then
            local tiles = get_sector_tanks(mem_addr["s2_tanks"]["offset"], mem_addr["s2_tanks"]["len"])

            --debug
            for offset,data in pairs(tiles) do
                debug_log("s2tank-loop",string.format("%x",offset).."=")
                debug_log("s2tank-loop",data["roomid"])
                debug_log("s2tank-loop",data["tankid"])
                debug_log("s2tank-loop",data["coords"])

                if data["roomid"] ~= 0xFF and data["tankid"] ~= 0xFF and data["coords"] ~= 0xFFFF then
                    update_tile(segment,"sector_2",tiles,data["roomid"],data["tankid"],data["coords"])
                end
            end
        end
    end
end

function _update_s3_tanks(segment)
    debug_log("callback","update s3 tank")
    debug_log("s0tanks: area",area)
    if maplayout then
        if is_in_game() and area == 3 then
            local tiles = get_sector_tanks(mem_addr["s3_tanks"]["offset"], mem_addr["s3_tanks"]["len"])

            --debug
            for offset,data in pairs(tiles) do
                debug_log("s3tank-loop",string.format("%x",offset).."=")
                debug_log("s3tank-loop",data["roomid"])
                debug_log("s3tank-loop",data["tankid"])
                debug_log("s3tank-loop",data["coords"])

                if data["roomid"] ~= 0xFF and data["tankid"] ~= 0xFF and data["coords"] ~= 0xFFFF then
                    update_tile(segment,"sector_3",tiles,data["roomid"],data["tankid"],data["coords"])
                end
            end
        end
    end
end

function _update_s4_tanks(segment)
    debug_log("callback","update s4 tank")
    debug_log("s0tanks: area",area)
    if maplayout then
        if is_in_game() and area == 4 then
            local tiles = get_sector_tanks(mem_addr["s4_tanks"]["offset"], mem_addr["s4_tanks"]["len"])

            --debug
            for offset,data in pairs(tiles) do
                debug_log("s4tank-loop",string.format("%x",offset).."=")
                debug_log("s4tank-loop",data["roomid"])
                debug_log("s4tank-loop",data["tankid"])
                debug_log("s4tank-loop",data["coords"])

                if data["roomid"] ~= 0xFF and data["tankid"] ~= 0xFF and data["coords"] ~= 0xFFFF then
                    update_tile(segment,"sector_4",tiles,data["roomid"],data["tankid"],data["coords"])
                end
            end
        end
    end
end

function _update_s5_tanks(segment)
    debug_log("callback","update s5 tank")
    debug_log("s0tanks: area",area)
    if maplayout then
        if is_in_game() and area == 5 then
            local tiles = get_sector_tanks(mem_addr["s5_tanks"]["offset"], mem_addr["s5_tanks"]["len"])

            --debug
            for offset,data in pairs(tiles) do
                debug_log("s5tank-loop",string.format("%x",offset).."=")
                debug_log("s5tank-loop",data["roomid"])
                debug_log("s5tank-loop",data["tankid"])
                debug_log("s5tank-loop",data["coords"])

                if data["roomid"] ~= 0xFF and data["tankid"] ~= 0xFF and data["coords"] ~= 0xFFFF then
                    update_tile(segment,"sector_5",tiles,data["roomid"],data["tankid"],data["coords"])
                end
            end
        end
    end
end

function _update_s6_tanks(segment)
    debug_log("callback","update s6 tank")
    debug_log("s0tanks: area",area)
    if maplayout then
        if is_in_game() and area == 6 then
            local tiles = get_sector_tanks(mem_addr["s6_tanks"]["offset"], mem_addr["s6_tanks"]["len"])

            --debug
            for offset,data in pairs(tiles) do
                debug_log("s6tank-loop",string.format("%x",offset).."=")
                debug_log("s6tank-loop",data["roomid"])
                debug_log("s6tank-loop",data["tankid"])
                debug_log("s6tank-loop",data["coords"])

                if data["roomid"] ~= 0xFF and data["tankid"] ~= 0xFF and data["coords"] ~= 0xFFFF then
                    update_tile(segment,"sector_6",tiles,data["roomid"],data["tankid"],data["coords"])
                end
            end
        end
    end
end

function reset_all(flag)
    if not flag then
        debug_log("resetall", "Clearing Bosses")
        for k,v in pairs(boss) do
            local location = Tracker:FindObjectForCode(boss[k]["location"])
            local item     = Tracker:FindObjectForCode(k)
            debug_log("resetall", k)
            location.Sections.Name.AvailableChestCount = 1
            item.Active = false
        end

        debug_log("resetall", "Clearing Item Locations")
        for k1,v1 in pairs(item_locations) do
            if k1 ~= "tank_id" then
                for k2,v2 in pairs(item_locations[k1]) do
                    for k3,v3 in pairs(v2) do
                        debug_log("resetall",v3)
                        local location = Tracker:FindObjectForCode(v3)
                        location.Sections.Name.AvailableChestCount = 1
                    end
                end
            end
        end

        debug_log("resetall", "Resetting upgrades")
        for k in pairs(beam) do
            local item = Tracker:FindObjectForCode(k)
            debug_log("resetall", k)
            item.Active = false
        end
        for k in pairs(weapons) do
            local item = Tracker:FindObjectForCode(k)
            debug_log("resetall", k)
            item.Active = false
        end
        for k in pairs(suit) do
            local item = Tracker:FindObjectForCode(k)
            debug_log("resetall", k)
            item.Active = false
        end
        for k in pairs(security) do
            local item = Tracker:FindObjectForCode(k)
            debug_log("resetall", k)
            item.Active = false
        end
        for k in pairs(minor_items) do
            local item = Tracker:FindObjectForCode(k)
            debug_log("resetall", k)
            item.AcquiredCount = 0
        end
        resetflag = true
    end
end
