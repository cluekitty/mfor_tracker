DEBUG_MODE = false

ScriptHost:LoadScript("scripts/functions.lua")
ScriptHost:LoadScript("scripts/memory.lua")

-- Autotracker Functions
function autotracker_started()
    debug_log("emotracker","autotracking init")
    init_autotracking()
end

function autotracker_stopped()
    debug_log("emotracker","autotracking halt")
    halt_autotracking()
end

function read_byte(addr)
    return AutoTracker:ReadU8(addr, 0)
end

function read_short(addr)
    return AutoTracker:ReadU16(addr, 0)
end

function read_long(addr)
    --  Emotracker does not seem to support 32-bit integers, need to do two 16-bit reads and concat into one 32-bit
    return AutoTracker:ReadU16(addr + 0x2, 0) << 16 | AutoTracker:ReadU16(addr, 0)
end

function create_mem_watch(name,addr,length,callback,rate)
    if not rate then
        rate = 1000
    end

    return ScriptHost:AddMemoryWatch(name,addr,length,callback,rate)
end

function remove_mem_watch(watch)
    ScriptHost:RemoveMemoryWatch(watch)
end

function init_autotracking()
    if not memwatch then
        print("AutoTracking Started")
        for k,v in pairs(mem_addr) do
            if k ~= "gameversion" then
                local name,offset,length,cb,rate = mem_addr[k]["name"],mem_addr[k]["offset"],mem_addr[k]["len"],mem_addr[k]["callback"],mem_addr[k]["rate"]
                mem_addr[k]["memwatch"] = create_mem_watch(name,offset,length,cb,rate)
                -- print(mem_addr[k]["memwatch"])
                -- debug_log("autotrackinit",k)
                -- debug_log("autotrackinit",name)
                -- debug_log("autotrackinit",offset)
                -- debug_log("autotrackinit",length)
                -- print("autotrackinitcb")
                -- print(cb)
                -- debug_log("autotrackinit",rate)
            end
        end
    end
    memwatch = true
end

function halt_autotracking()
    print("AutoTracking Halted")
    if memwatch then
        for k,v in pairs(mem_addr) do
            if k ~= "gameversion" then
                remove_mem_watch(mem_addr[k]["memwatch"])
            end
        end
    end
    memwatch = false
end

--[[ScriptHost Reference
    ScriptHost:LoadScript(str)
    ScriptHost:AddMemoryWatch(str,addr,addr_len,callback,period[default=1000])
    ScriptHost:RemoveMemoryWatch(addr)
    PushMarkdownNotification(type,message,timeout)
        type enum: NotificationType.Message, Celebration, Warning, Error
--]]

ScriptHost:AddMemoryWatch("Game Version", mem_addr["gameversion"]["offset"], 0x04, _check_game_version, 60 * 1000)
