local bit = M

function init()
    if initialized == nil then
        -- Used for automatically adding/removing missile/pb count when main data is collected, for security lock broadcast display mode, and settings.
        item_state_list = {
            ["missile_data"]            = false,
            ["morph_ball"]              = false,
            ["charge_beam"]             = false,
            ["bomb_data"]               = false,
            ["high_jump"]               = false,
            ["speed_booster"]           = false,
            ["super_missile_data"]      = false,
            ["varia_suit"]              = false,
            ["ice_missile_data"]        = false,
            ["wide_beam"]               = false,
            ["power_bomb_data"]         = false,
            ["space_jump"]              = false,
            ["plasma_beam"]             = false,
            ["gravity_suit"]            = false,
            ["diffusion_missile_data"]  = false,
            ["wave_beam"]               = false,
            ["screw_attack"]            = false,
            ["ice_beam"]                = false,
            ["security_1"]              = false,
            ["security_2"]              = false,
            ["security_3"]              = false,
            ["security_4"]              = false,
            ["setting_oob"]             = false,
            ["setting_damage_runs"]     = false,
            ["setting_split_security"]  = true,
        }
        doors = {
            ["blue"]   = Tracker:FindObjectForCode("security_1"),
            ["green"]  = Tracker:FindObjectForCode("security_2"),
            ["yellow"] = Tracker:FindObjectForCode("security_3"),
            ["red"]    = Tracker:FindObjectForCode("security_4"),
        }
        broadcast_doors = {
            ["blue"]   = Tracker:FindObjectForCode("broadcast_lock_1"),
            ["green"]  = Tracker:FindObjectForCode("broadcast_lock_2"),
            ["yellow"] = Tracker:FindObjectForCode("broadcast_lock_3"),
            ["red"]    = Tracker:FindObjectForCode("broadcast_lock_4"),
        }

        local item = Tracker:FindObjectForCode("setting_split_security")
        item.Active = item_state_list["setting_split_security"]

        initialized = true
    end
end

function tracker_on_accessibility_updated()
    -- items we care about
    local item_list = {
        "missile_data",
        "power_bomb_data",
        "security_1",
        "security_2",
        "security_3",
        "security_4",
        "setting_oob",
        "setting_damage_runs",
        "setting_split_security",
    }

    if initialized and not memwatch then
        for i = 1, #item_list do
            local item = Tracker:FindObjectForCode(item_list[i])
            local state = item_state_list[item_list[i]]
            item_check(item,state)
        end
    end
end

function _update_missile_count(item)
    if not memwatch then
        local missile_tank = Tracker:FindObjectForCode("missile_tank")

        if item.Active then
            debug_log("callback", "add 10 missiles to missile_tank")
            missile_tank:Increment(2) -- 2 increments of 5 missiles
        else
            debug_log("callback", "subtract 10 missiles from missile_tank")
            missile_tank:Decrement(2) -- 2 increments of 5 missiles
        end
        item_state_list["missile_data"] = item.Active
    end
end

function _update_power_bomb_count(item)
    if not memwatch then
        local pb_tank = Tracker:FindObjectForCode("pb_tank")
        if item.Active then
            debug_log("_update_power_bomb_count()", "add 10 pbombs to pb_tank")
            pb_tank:Increment(5) -- 5 increments of 2 pbombs
        else
            debug_log("callback", "subtract 10 pbombs from pb_tank")
            pb_tank:Decrement(5) -- 5 increments of 2 pbombs
        end
        item_state_list["power_bomb_data"] = item.Active
    end
end

function _update_level_1_door_lock(item)
    if item_state_list["setting_split_security"] == true then
        broadcast_doors["blue"].Active   = item.Active
        item_state_list["security_1"]    = item.Active
    else
        doors["blue"].Active             = item.Active
        broadcast_doors["blue"].Active   = item.Active
        item_state_list["security_1"]    = item.Active

        doors["green"].Active            = false
        broadcast_doors["green"].Active  = false
        item_state_list["security_2"]    = false

        doors["yellow"].Active           = false
        broadcast_doors["yellow"].Active = false
        item_state_list["security_3"]    = false

        doors["red"].Active              = false
        broadcast_doors["red"].Active    = false
        item_state_list["security_4"]    = false
    end
end

function _update_level_2_door_lock(item)
    if item_state_list["setting_split_security"] == true then
        broadcast_doors["green"].Active  = item.Active
        item_state_list["security_2"]    = item.Active
    else
        doors["blue"].Active             = item.Active
        broadcast_doors["blue"].Active   = item.Active
        item_state_list["security_1"]    = item.Active

        doors["green"].Active            = item.Active
        broadcast_doors["green"].Active  = item.Active
        item_state_list["security_2"]    = item.Active

        doors["yellow"].Active           = false
        broadcast_doors["yellow"].Active = false
        item_state_list["security_3"]    = false

        doors["red"].Active              = false
        broadcast_doors["red"].Active    = false
        item_state_list["security_4"]    = false
    end
end

function _update_level_3_door_lock(item)
    if item_state_list["setting_split_security"] == true then
        broadcast_doors["yellow"].Active = item.Active
        item_state_list["security_3"]    = item.Active
    else
        doors["blue"].Active             = item.Active
        broadcast_doors["blue"].Active   = item.Active
        item_state_list["security_1"]    = item.Active

        doors["green"].Active            = item.Active
        broadcast_doors["green"].Active  = item.Active
        item_state_list["security_2"]    = item.Active

        doors["yellow"].Active           = item.Active
        broadcast_doors["yellow"].Active = item.Active
        item_state_list["security_3"]    = item.Active

        doors["red"].Active              = false
        broadcast_doors["red"].Active    = false
        item_state_list["security_4"]    = false
    end
end

function _update_level_4_door_lock(item)
    if item_state_list["setting_split_security"] == true then
        broadcast_doors["red"].Active    = item.Active
        item_state_list["security_4"]    = item.Active
    else
        doors["blue"].Active             = item.Active
        broadcast_doors["blue"].Active   = item.Active
        item_state_list["security_1"]    = item.Active

        doors["green"].Active            = item.Active
        broadcast_doors["green"].Active  = item.Active
        item_state_list["security_2"]    = item.Active

        doors["yellow"].Active           = item.Active
        broadcast_doors["yellow"].Active = item.Active
        item_state_list["security_3"]    = item.Active

        doors["red"].Active              = item.Active
        broadcast_doors["red"].Active    = item.Active
        item_state_list["security_4"]    = item.Active
    end
end

function _update_split_security_setting(item)
    item_state_list["setting_split_security"] = item.Active
    debug_log("callback", "security: " .. tostring(item_state_list["setting_split_security"]))
end

function _update_damage_runs_setting(item)
    item_state_list["setting_damage_runs"] = item.Active
    debug_log("callback", "damage: " .. tostring(item_state_list["setting_damage_runs"]))
end

function _update_oob_setting(item)
    item_state_list["setting_oob"] = item.Active
    debug_log("callback", "oob: " .. tostring(item_state_list["setting_oob"]))
end

function item_check(item, prev_state)
    -- Update item state table when registered items are toggled
    -- between active and inactive.

    if item.Active == prev_state then
        debug_log("item_check", "item: " .. item.Name .. "-" .. tostring(item.Active))
        return
    elseif item.CurrentStage == prev_state then
        return
    end

    item_to_update_function = {
        ["Missile Data"]          = _update_missile_count,
        ["Power Bombs"]           = _update_power_bomb_count,
        ["Level 1 Door Lock"]     = _update_level_1_door_lock,
        ["Level 2 Door Lock"]     = _update_level_2_door_lock,
        ["Level 3 Door Lock"]     = _update_level_3_door_lock,
        ["Level 4 Door Lock"]     = _update_level_4_door_lock,
        ["Split Security Toggle"] = _update_split_security_setting,
        ["Damage Runs Toggle"]    = _update_damage_runs_setting,
        ["OOB Toggle"]            = _update_oob_setting,
    }

    local callback = item_to_update_function[item.Name]

    if not callback then
        debug_log("item_check", "item " .. item.Name .. " has no callback function")
        return
    end

    callback(item)
end

function debug_log(x, str)
    if DEBUG_MODE == true then
        print("DEBUG: "..x..": "..tostring(str))
    end
end
