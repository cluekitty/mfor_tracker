-- Load Tracker Items

Tracker:AddItems("items/beams.json")
Tracker:AddItems("items/boss.json")
Tracker:AddItems("items/security_doors.json")
Tracker:AddItems("items/missiles.json")
Tracker:AddItems("items/morph_items.json")
Tracker:AddItems("items/suits.json")
Tracker:AddItems("items/items.json")
Tracker:AddItems("items/settings.json")

maplayout = string.find(Tracker.ActiveVariantUID, "standard_with_map")

if (maplayout) then
    Tracker:AddMaps("maps/maps.json")
    ScriptHost:LoadScript("scripts/logic.lua")
    Tracker:AddLocations("locations/logic.json")
    Tracker:AddLocations("locations/settings.json")
    --  always load map locations last
    for n=0, 6 do
        Tracker:AddLocations("locations/sectors/sector_"..n..".json")
    end
end

ScriptHost:LoadScript("scripts/helper.lua")

Tracker:AddLayouts("layouts/bosses.json")
Tracker:AddLayouts("layouts/items.json")
Tracker:AddLayouts("layouts/settings.json")
Tracker:AddLayouts("layouts/tracker.json")
Tracker:AddLayouts("layouts/broadcast_layout.json")

ScriptHost:LoadScript("scripts/autotracking.lua")

init()
