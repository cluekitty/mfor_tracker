# Description

This is an item tracker for Metroid Fusion to use with [EmoTracker](https://emotracker.net).

## Special Thanks

ChaosMiles07, Ridleymaster for the original SM icons (https://www.spriters-resource.com/custom_edited/metroidcustoms/sheet/23198/) (used for High-jump boots). Note, while this icon still exists within the pack, it is no longer used and has been replaced with a sprite ripped from the game. Those who wish to use it can modify the source on items/items.json on line 36.

Ultinaruto & Jakoliath for the original versions of this pack and for ripping/creating several assets for bosses. Namely Nettori, Nightmare, X-B.O.X., and Ridley.

## Assets
All other assets have either been ripped directly from the game's ROM, been ripped through screenshots of emulated gameplay, or created using a combination of the first two.
Several items have alternate versions for sprites.
