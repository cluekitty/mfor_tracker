1.2.3
---
Layout Adjustments
- Fixed Sector 2 Map where connections on right side were incorrect.
- Added "Full" Map which displays all sectors by default.

Access Rules
- Fixed incorrect function name usage in several files ($missing -> $missing_item)
- Added Pump Control location for some Sector 4 checks.
- Fixed Access rules for the following locations.
  - S0-13-07 - Removed Screw Attack as possible access rule.
  - S0-13-08 - Same as S0-13-07
  - S1-09-04 - Same as S1-0D-08.
  - S1-0D-08 - Fixed access rules incorrectly displaying item as accessible when missing items required to traverse the sector.
  - S1-11-02 - Same as S1-0D-08.
  - S2-03-0C
  - S2-04-0B
  - S2-05-08 - Fixed access rules showing incorrect state when user had High Jump but not Space Jump + Plasma Beam.
  - S2-05-0A - Same as S2-05-08.
  - S3-00-05 - Added access rules for checking without Screw Attack.
  - S3-0F-00 - Fixed access rules missing Morph Ball requirement, and additionally Bombs requirement if accessing with Level 2 security.
  - S3-0B-06
  - S3-11-04 - Added Charge Beam as a possible requirement for defeating BOX first encounter.
  - S3-11-0A - Removed extraneous condition.
  - S3-12-03 (Data Room) - Added charge beam as possible requirement for accessing Data room as BOX fight does not require missiles.
  - S4-12-07 - Fixed badge display color when item is available. Fixed item accessibility check expecting you to have damage runs on.
  - S4-13-07 - Fixed item accessibility check expecting you to have damage runs on.
  - S5-03-04 - Fixed access rules missing gravity_suit requirement for underwater screw attack in sector 4.
  - S5-04-01 - Same as S5-03-04.
  - S5-05-01 - Fixed access rules incorrectly assuming that sector 5 must be in a destroyed state to be accessible.
  - S5-06-05 - Fixed access rules missing requirement for destroying Power Bomb Geron.

1.2.2
---
Created horizonal and vertical layouts for window size adjustments.

Reduced size of items on item-only tracking to better suit smaller screens.

Fixed Access rules for the following locations:
- S0-05-16 - Fixed incorrect claimability when missing a required item.
- S0-09-04 - Fixed visibility rules for when you can see the item but not obtain it.
- S0-0E-0B - Fixed visibility rules for when you can see the item but not obtain it.
- S0-15-10 (again)
- S0-16-12 (again)
- Yakuza (again)
- Fixed Upper Sector 1 access missing Charge-beam as a possible requirement for damaging stabilizer gerons.
- S1-0C-07 - Fixed visibility rules for when you can see the item but not obtain it.
- S2-04-03 - Fixed claimability for when you have a powerup that lets you obtain the item.
- S3-03-08 - Fixed incorrect claimability when missing a required item.
- S3-0A-01 - Fixed visibility rules incorrectly showing item as viewable/obtainable when it was not.
- S3-0B-06 - Fixed incorrect claimability when missing a required item.
- S3-11-04 - Fixed access missing power bombs as a possible requirement for damaging Super Missile Gerons.
- S4-0F-06 - Added additional item for checking location. Removed damage run settings from this section for now as they would always show up as a yellow item with damage run
- S5-0E-08 - Fixed visibilty rules for when you can see the item but not obtain it.
- S5-0C-05 - Fixed incorrect accessibility and claimability rules.
- S6-0E-03 and S6-0E-04 - Reversed Access rules for these items as they were incorrect.


1.2.1
---
Fixed some Access rules which were incorrect
- S0-15-10
- S0-16-12
- Yakuza
- S4-12-07
- S4-13-07

Removed background color from Broadcast Layout for use with NDI Capture.

Fixed AutoTracking issues with S2-00-05.

Added new High-Jump Boots icon to replace sourced image.

1.2.0
---
Added Autotracking

1.1.0
---
- Add togglable settings in tracker.
- Fix some access rules.
- Refactor lua to use callbacks for updating certain information.

1.0.1
---
Fixed access rules in Sector 1 at the following locations from bug report.
- S1-05-03
- Charge Core-X
- S1-0D-08

Normalized some access rules with inconsistent item codes.
Renamed bosses to better reflect randomizer's spoiler logs.

1.0.0
---
First release
- Standard Tracker Layout
- Tracker Layout with Map
- Broadcast Layout
- Map layout has some logic which may be incomplete
